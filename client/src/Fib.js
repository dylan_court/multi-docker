import React, {Component} from 'react';
import axios from 'axios';

class Fib extends Component {
    state = {
        seenIndexes: [],
        values: {},
        index: ''
    };

    componentDidMount(){
        this.fetchValues();
        this.fetchIndexes();
    }

    // Retreive values from Redis via Express
    async fetchValues(){
        const values = await axios.get('/api/values/current');
        this.setState({values: values.data});
    }

    // Retreive values from Postgres via Express
    async fetchIndexes(){
        const seenIndexes = await axios.get('/api/values/all');
        this.setState({
            seenIndexes: seenIndexes.data
        });
    }

    // Submit user input as POST to Postgres/Redis via Express
    handleSubmit= async event => {
        event.preventDefault();

        await axios.post('/api/values',{
            index: this.state.index
        });
        this.setState({index: ''});
    };

    // Display Postgres data as Array
    renderSeenIndexes(){
        return this.state.seenIndexes.map(({ number }) => number).join(', ');
    }

    // Display Redis data as rows
    renderValues() {
        const entries = [];
        for (let key in this.state.values){
            entries.push(
                <div key={key}>
                    For index {key} I calculated {this.state.values[key]}
                </div>
            );
        } 
        
        return entries;
    }

    // Display page with form for user input, Index array and Values rows
    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>Enter your index:</label>
                    <input
                        value={this.state.index}
                        onChange={event => this.setState({index: event.target.value})}                    
                    />
                    <button>Submit</button>
                </form>
                <h3>Indexes I have seen:</h3>
                {this.renderSeenIndexes()}
                <h3>Calculated values:</h3>
                {this.renderValues()}
            </div>
        );
    }
}

export default Fib;